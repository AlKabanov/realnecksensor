#include <em_cmu.h>
#include <em_emu.h>

#include <em_gpio.h>
#include <em_usart.h>
#include "spi.h"

#define USART_USED                USART1
#define USART_LOCATION            USART_ROUTE_LOCATION_LOC1
#define USART_CLK                 cmuClock_USART1
#define PIN_SPI_TX                0			//MOSI = PD0
#define PORT_SPI_TX               gpioPortD
#define PIN_SPI_RX                1			//MISO = PD1
#define PORT_SPI_RX               gpioPortD
#define PIN_SPI_CLK               2			//SCK = PD2
#define PORT_SPI_CLK              gpioPortD


void spiSetup()
{
	USART_InitSync_TypeDef init = USART_INITSYNC_DEFAULT;

	/* Enable module clocks */
	CMU_ClockEnable(cmuClock_HFPER, true);
	CMU_ClockEnable(USART_CLK, true);
	CMU_ClockEnable(cmuClock_GPIO, true);

	/* Configure SPI pins */
	GPIO_PinModeSet(PORT_SPI_TX, PIN_SPI_TX, gpioModePushPull, 0);
	GPIO_PinModeSet(PORT_SPI_RX, PIN_SPI_RX, gpioModeInputPull, 0);
	GPIO_PinModeSet(PORT_SPI_CLK,PIN_SPI_CLK,gpioModePushPull, 0);
	

	/* Reset USART just in case */
	USART_Reset(USART_USED);

	init.clockMode = usartClockMode0;

	/* Configure to use SPI master with manual CS */
	init.refFreq = 0;
	init.baudrate = 1000000;
	init.msbf = true;	/* Send most significant bit first. */
	USART_InitSync(USART_USED, &init);

	/* Enabling pins and setting location, SPI CS not enable */
	USART_USED->ROUTE = USART_ROUTE_TXPEN | USART_ROUTE_RXPEN | USART_ROUTE_CLKPEN | USART_LOCATION;
}

// perform SPI transaction with radio
uint8_t spiSend (uint8_t out)
{
	/* For every byte sent, one is received */
	USART_Tx(USART_USED, out);
	return USART_Rx(USART_USED);
}

