#include <em_cmu.h>
#include <em_emu.h>
#include <stdbool.h>
#include <em_leuart.h>
#include <em_gpio.h>
#include <intrinsics.h>
#include "radio.h"
#include "emul.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/
#define PULSE_NO_CONNECTION 0
#define PULSE_FAILURE 1
#define PULSE_NO_MEASURE 2
#define BYTEHIGH(v)   (*(((unsigned char *) (&v) + 1)))
#define BYTELOW(v)  (*((unsigned char *) (&v)))

typedef struct{
  uint32_t address;
  uint32_t counter;
  uint8_t  type;
  uint8_t  cowCond;
  uint8_t  cowActivity[4];
  uint8_t  inT;
  uint8_t  outTLow;
  uint8_t  outTHigh;
  uint8_t  temp3;
  uint8_t  temp4;
  uint8_t  pulse;
  uint8_t  aes128[12];
} message_t;



/*******************************************************************************
 ***************************   static variables*********************************
 ******************************************************************************/
static message_t message;
static uint32_t sensorAddress;
static uint8_t inT;
static uint16_t outT;
//static uint8_t messageLen = sizeof(message);
/*******************************************************************************
 ********************   LOCAL FUNCTIONS PROTOTYPES  ****************************
 ******************************************************************************/

/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/

/*******************************************************************************
 * @brief address setter
 *   
 * @param[in] address - address of sensor
 *  
 ******************************************************************************/
void emulSetAddr (uint32_t address)
{
  sensorAddress = address;
}
/*******************************************************************************
 * @brief inner temperature setter
 *   
 * @param[in] temperature - inner temperature
 *  
 ******************************************************************************/
void emulSetInT (uint8_t temperature)
{
  inT = temperature;
}
/*******************************************************************************
 * @brief outer temperature setter
 *   
 * @param[in] temperature - inner temperature
 *  
 ******************************************************************************/
void emulSetOutT (uint16_t temperature)
{
  outT = temperature;
}


/***************************************************************************//**
 * @brief create message for sending
 *   
 * @param[in] counter - messages counter
 *  
 * @param[in] data - reference to array for message
 *
 * @param[in] dataLen - length of array
 *
 * @return   true if time to send message
 *   
 ******************************************************************************/
void emulMessage(uint32_t counter,uint8_t* data, uint8_t dataLen)
{
 
    message.address = sensorAddress;
    message.counter = counter;
    message.type = 0;
    message.cowCond = 0xFF;  //no defined
    message.cowActivity[0] = 0;
    message.cowActivity[1] = 0;
    message.cowActivity[2] = 0;
    message.cowActivity[3] = 0;
    message.inT = inT;
    message.outTHigh = BYTEHIGH(outT);
    message.outTLow = BYTELOW(outT);
    message.temp3 = 0;
    message.pulse = PULSE_FAILURE; 
    for(int i = 0; i < dataLen;i++)data[i] = (*(((uint8_t *) (&message) + i)));  //send bytes from struct to array

}


/*******************************************************************************
 **************************   LOCAL FUNCTIONS   *******************************
 ******************************************************************************/

