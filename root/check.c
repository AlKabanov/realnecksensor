/**************************************************************************//**
 * @file
 * @brief checking messages from Base Station
 * @version 1.0.1
 ******************************************************************************
 *
 ******************************************************************************/
#include "main.h"
#include "radio.h"
#include "check.h"
/*******************************************************************************
* @brief
 *   Check if recieved message is wake up messge and has right address
 *
 * @param[in] address 
 *    addres of transmitter
 * @param[in] message
 *   addres of received message
 *
 * @return
 *   true if message is valid
********************************************************************************/
bool checkWakeUp(uint8_t address, receive_t *message)
{
  if(message->buffer[0] != WAKE_MESS)return false;
  if(!address || address > 5)return false;  //addresses may be 1 2 3 4 5
  if(message->buffer[1] & (0x01 << (address-1)))return true;
  return false;
}
/*******************************************************************************
* @brief
 *   Check if recieved message is go to tx rumination 
 *   messge and has right address
 *
 * @param[in] address 
 *    addres of transmitter
 * @param[in] message
 *   addres of received message
 *
 * @return
 *   true if message is valid
********************************************************************************/     
bool checkTXRum(uint8_t address, receive_t *message)
{
  if(message->buffer[0] != START_MESS)return false;
  if(!address || address > 5)return false;  //addresses may be 1 2 3 4 5
  if(message->buffer[1] & (0x01 << (address-1)))return true;
  return false;
}
/*******************************************************************************
* @brief
 *   Check if recieved message is stop
 *   messge and has right address
 *
 * @param[in] address 
 *    addres of transmitter
 * @param[in] message
 *   addres of received message
 *
 * @return
 *   true if message is valid
********************************************************************************/
bool checkStop(uint8_t address, receive_t *message)
{
  if(message->buffer[0] != STOP_MESS)return false;
  if(!address || address > 5)return false;  //addresses may be 1 2 3 4 5
  if(message->buffer[1] & (0x01 << (address-1)))return true;
  return false;
}
/*******************************************************************************
* @brief
 *   Check if recieved message is "is alive"
 *   messge and has right address
 *
 * @param[in] address 
 *    addres of transmitter
 * @param[in] message
 *   addres of received message
 *
 * @return
 *   true if message is valid
********************************************************************************/
bool checkIsAlive(uint8_t address, receive_t *message)
{
  if(message->buffer[0] != ISALIVE_MESS)return false;
  if(!address || address > 5)return false;  //addresses may be 1 2 3 4 5
  if(message->buffer[1] & (0x01 << (address-1)))return true;
  return false;
}
/*******************************************************************************
* @brief
 *   find frequency band for transmition
 *
 * @param[in] address 
 *    addres of transmitter
 * @return
 *   frequency band
********************************************************************************/
band_t txBand(uint8_t address)
{
  switch (address)
  {
      case 1: return band0;break;
      case 2: return band1;break;
      case 3: return band2;break;
      case 4: return band3;break;
      case 5: return band4;break;
    default : return band0;
  }
}
