/**************************************************************************//**
 * @file
 * @brief firmware for necksensor for finding rumination detection algorithm
 * @version 1.0.1
 ******************************************************************************
 *
 ******************************************************************************/


#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rtc.h"
//#include "mc3635SPI.h"

#include "main.h"
#include "gpio.h"
#include "rtc.h"
#include "spi.h"
#include "radio.h"
#include "adc.h"
#include "uart.h"
//#include "mc3635.h"
#include "i2c.h"
#include "check.h"
#include "linear.h"
#include "emul.h"

#define POWER 10
#define ONE_SEC  (32768)
#define TWO_SEC  ((ONE_SEC*2)-1)
#define MSEC_500 ((ONE_SEC/2)-1)
#define MSEC_2 ((ONE_SEC/500)-1)
   
#define TX_ADDR 1
#define HOUR 9


//#define LORA_TEST 
//#define MC3635_TEST
//#define ADC_TEST


uint8_t data[32];               // 2 - crc
receive_t RADIO_IN = {20,0,0};	//default period = 20mS, dataPtr = 0,lastSent=0,dataValid=0
readStruct_t fromMC3635;        //, tempFromMC3635;
uint32_t sendCounter = 0;
uint32_t hourCounter = 0, time;
int16_t inT = 350, outT = 0;
uint8_t temperatureTimer = 0;
int debugPin1 = 0, debugPin2 = 0;



/*******************************************************************************
private function prototypes
*******************************************************************************/
void sleepMode(void);

/**************************************************************************//**
 * @brief Update clock and wait in EM2 for RTC tick.
 *****************************************************************************/
void clockLoop(void)
{
  
  //gpioLDOOn();
#ifdef LORA_TEST
  data[0] = READY_MESS;
  data[1] = TX_ADDR;
  while(1)
  {
    test = radioTest(data, 4,POWER,false,band0);
    rtcWait(2000);
  }
#endif
#ifdef MC3635_TEST
  while(1)
  {
    if(i2cGetFIFOintFlag() == true)
    {
      i2cAccDataRead(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES); //read from FIFO 28 samples
    }
  }
#endif
#ifdef ADC_TEST
 
  gpioTEMPPWRON();
  
  while(1)
  {
    
    inT = (adcGetTemperature(1) - 40000)/10;  
    outT = (adcGetTemperature(2) - 40000)/10;
    
    __no_operation();
  }
#endif
  gpioTEMPPWRON();
  inT= (adcGetTemperature(1) - 40000)/10;  
  outT = (adcGetTemperature(2) - 40000)/10;
  gpioTEMPPWROFF();
  
//  radioRXCon(STANDBY_BAND);
  emulSetAddr(TX_ADDR);
  rtcSetWakeUp(ONE_SEC*5-1);  //sleep for 5 sec
  time = rtcGetMS();
  while (1)
  {
     if(i2cGetFIFOintFlag() == true)
     {
        i2cAccDataRead(&fromMC3635.xBuf[0],&fromMC3635.yBuf[0],&fromMC3635.zBuf[0],PAGE_SAMPLES); //read from FIFO 28 samples
     }
     if(time < rtcGetMS())
     {
       if(++hourCounter > HOUR)
       {
         hourCounter = 0;
         //NVIC_EnableIRQ(GPIO_ODD_IRQn);
         //NVIC_EnableIRQ(GPIO_EVEN_IRQn);
         gpioTEMPPWRON();
         debugPin1 = GPIO_PinInGet(gpioPortF, 3);       
         for(int i = 15000; i > 0 ; i--);//wait some time to stabilize the power
         
         inT = (adcGetTemperature(1) - 40000)/10;  
         outT = (adcGetTemperature(2) - 40000)/10;
         inT = linearMake(inT);
         outT = linearMake(outT);
         if((inT < 160)||(inT > 414))inT = 0;
         else inT = inT - 159;
         if((outT < -200)||(outT > 500))outT = 0;
         else outT = outT + 201;
           
         gpioTEMPPWROFF();
         debugPin2 = GPIO_PinInGet(gpioPortF, 3);
         emulSetInT((uint8_t)inT);
         emulSetOutT((uint16_t)outT);
         sendCounter++;
         
         emulMessage(sendCounter, data, sizeof(data));
         
         radioTest(data, sizeof(data), POWER, false, txBand(TX_ADDR));
         while(!getTXDoneFlag()); //wait for transmission to finish
         radioSleep();
         for(int i = 15000; i > 0 ; i--);//wait some time to sleep;
         
         //NVIC_DisableIRQ(GPIO_ODD_IRQn);
         //NVIC_DisableIRQ(GPIO_EVEN_IRQn);
       }   
       time = rtcGetMS();
     }
     sleepMode();   
  }
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
  /* Chip errata */
  CHIP_Init();

  /* Ensure core frequency has been updated */
  SystemCoreClockUpdate();


  /* Setup RTC to generate an interrupt every minute */
  rtcSetup();  

  /* Setup GPIO interrupt to set the time */
  gpioSetup();
  
  spiSetup();
  gpioRST(0);
  //gpioSetLED();
  rtcWait(200);
  //gpioClearLED();
  gpioRST(1);
  gpioI2CPWRON();
  adcInit();
#ifndef LORA_TEST
  i2cMC3635Init();
#endif
  //MC3635SPIInit();
  //uartInit(9600, true);
  /* Main function loop */
  clockLoop();

  return 0;
}


void sleepMode(void)
{
  //gpioPULSEPWROFF();
  
  RTC_IntClear(RTC_IFC_COMP0);
  RTC_IntDisable(RTC_IEN_COMP0 );
  //NVIC_DisableIRQ(GPIO_ODD_IRQn);
  RTC_IntEnable(RTC_IEN_COMP0 );
  EMU_EnterEM2(true);
  
}