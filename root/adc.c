#include <em_chip.h>
#include <em_emu.h>
#include <em_cmu.h>
#include <em_adc.h>
#include "adc.h"

#define SAMP_NUM 10

void adcInit(void)
{
  ADC_Init_TypeDef       init       = ADC_INIT_DEFAULT;
  
  CMU_ClockEnable(cmuClock_ADC0, true);

  /* Init common settings for both single conversion and scan mode */
  init.timebase = ADC_TimebaseCalc(0);
  /* Might as well finish conversion as quickly as possibly since polling */
  /* for completion. */
  /* Set ADC clock to 7 MHz, use default HFPERCLK */
  init.prescale = ADC_PrescaleCalc(7000000, 0);
  
  /* Set ADC clock to 400 kHz, use default HFPERCLK */
  //init.prescale = ADC_PrescaleCalc(400000, 0);
  

  /* Set oversampling rate */
  init.ovsRateSel = adcOvsRateSel32;

  /* WARMUPMODE must be set to Normal according to ref manual before */
  /* entering EM2. In this example, the warmup time is not a big problem */
  /* due to relatively infrequent polling. Leave at default NORMAL, */

  ADC_Init(ADC0, &init);

  
  
/* Base the ADC configuration on the default setup. */
//  ADC_Init_TypeDef init  = ADC_INIT_DEFAULT;
//  
//  init.timebase = ADC_TimebaseCalc(0);
//  init.prescale = ADC_PrescaleCalc(400000, 0);  //adc clock = 400kHz
//  CMU_ClockEnable(cmuClock_ADC0, true);
//  ADC_Init(ADC0, &init);
}

int32_t adcGetTemperature(uint8_t channel)
{
  ADC_InitSingle_TypeDef singleInit = ADC_INITSINGLE_DEFAULT;
  
  int32_t result = 0;
  
  /* Init for single conversion use, measure VDD/3 with 1.25 reference. */
  singleInit.reference = adcRefVDD;
  if(channel == 1)singleInit.input = adcSingleInpCh6;
  else singleInit.input = adcSingleInpCh7;

  /* Enable oversampling rate */
  singleInit.resolution = adcResOVS;

  /* The datasheet specifies a minimum aquisition time when sampling vdd/3 */
  /* 32 cycles should be safe for all ADC clock frequencies */
  singleInit.acqTime = adcAcqTime32;


  ADC_InitSingle(ADC0, &singleInit);
  
  
  /* Reference must be VCC */
//  single_init.reference = adcRefVDD;
//  single_init.resolution = adcRes12Bit;
//  single_init.input   	= adcSingleInpCh7;
//  ADC_InitSingle(ADC0, &single_init);
  
  
 //ommit first measurement
  
  // Start one ADC sample
  ADC_Start(ADC0, adcStartSingle);
  /* Wait while conversion is active */
  while (ADC0->STATUS & ADC_STATUS_SINGLEACT) ;
  for(int i = 0; i < SAMP_NUM; i++)
  {
     // Start one ADC sample
    ADC_Start(ADC0, adcStartSingle);
    /* Wait while conversion is active */
    while (ADC0->STATUS & ADC_STATUS_SINGLEACT) ; 
    result += ADC_DataSingleGet(ADC0);
  }
   
  return (result/SAMP_NUM);
  
  
  //CMU_ClockEnable(cmuClock_ADC0, false); //try to do this
  //return out;
}
