#ifndef MAIN_H
#define MAIN_H

#include <stdint.h>
#include <stdbool.h>
#include <intrinsics.h>

#define PAGE_SAMPLES 28
#define CHEW_MARK    5

#define WAKE_MESS 1
#define READY_MESS 2
#define START_MESS 3
#define STOP_MESS 4
#define GO2STANDBY_MESS 5
#define ISALIVE_MESS 6
#define ALIVE_MESS 7

#define BYTEHIGH(v)   (*(((unsigned char *) (&v) + 1)))
#define BYTELOW(v)  (*((unsigned char *) (&v)))

typedef struct{
  int16_t xBuf[PAGE_SAMPLES];
  int16_t yBuf[PAGE_SAMPLES];
  int16_t zBuf[PAGE_SAMPLES];
  uint8_t sessionNum;
  uint8_t chewing[PAGE_SAMPLES/CHEW_MARK];
} readStruct_t;


typedef union{
  readStruct_t readStruct;
  uint8_t writeBuf[0x100];
} write2Mem_t;

typedef enum 
{
  standby,   //waiting for swiching on
  wait4tx,
  txRum,
  txNormal,
} mode_t;


#endif /* MAIN_H */
